# CHALLENGE

Para ejecutar esta aplicación es necesario:

## Clonar el repositorio

```
git clone https://gitlab.com/221bytes1/challenge.git
```

## Crear archivo .env

Solo con copiar el .env.example funciona:

```
cp .env.example .env
```

## Instalar las dependencias

```
composer install
```

## Finalmente ejecutarlo

El proyecto se probó mediante el server interno de php. De todas maneras, incluye un archivo **.htaccess** para levantar el proyecto en un apache.

### Pruebas

Para ejecutar las pruebas:

```
./vendor/bin/phpunit
```
