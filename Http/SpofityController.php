<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

namespace Http;

use Exception;
use Models\Album;
use Dotenv\Dotenv;
use Exceptions\BadRequestException;
use Exceptions\NotFoundException;

class SpofityController {

    private $key;
    private $url_accounts;
    private $url_search;
    private $url_albums;
    private $access_token;
    private $token_type;
    private $expires_in;
    private $scope;

    public function __construct()
    {
        try {
            $dotenv = Dotenv::createImmutable( __DIR__ . './../');
            $dotenv->load();

            $dotenv->required(['KEY', 'URL_ACCOUNTS', 'URL_SEARCH', 'URL_ALBUMS'])->notEmpty();

            $this->key = $_ENV['KEY'];
            $this->url_accounts = $_ENV['URL_ACCOUNTS'];
            $this->url_search = $_ENV['URL_SEARCH'];
            $this->url_albums = $_ENV['URL_ALBUMS'];

        } catch ( Exception $e ){
            throw new Exception("No es posible acceder a la configuración de la aplicación", 
                                HttpCode::HTTP_INTERNAL_SERVER_ERROR );
        }
        
        $this->access_token = null;
        $this->token_type = null;
        $this->expires_in = null;
        $this->scope = null;
    }

    /**
     * Autentifica la key
     */
    public function callAccount() 
    {
    
        $curl = new CurlHelper( $this->url_accounts, $this->key, 'Basic' );

        // la siguiente llamada puede lanzar BadRequestException, 
        // pero la manejo en otro punto más adelante.

        $r = json_decode( $curl->post( "grant_type=client_credentials" ) );

        $this->access_token = $r->access_token;
        $this->token_type = $r->token_type;
        $this->expires_in = $r->expires_in;
        $this->scope = $r->scope;
        
        return $r;
    }


    /**
     * Obtiene el ID del artista buscado
     */
    public function getArtistID( $artista ) 
    {
        if( empty($artista) ){
            throw new NotFoundException();
        }

        $q = str_replace(' ', '%20', $artista);

        // verifico tener el token
        if( is_null($this->access_token)){
            $this->callAccount();
        }

        $curl = new CurlHelper( $this->url_search . $q . "&type=artist", $this->access_token );

        // la excepcion BadRequestException la manejo más adelante
        $response = $curl->get();

        $r = json_decode($response);
        if( isset($r->error) ){
            throw new BadRequestException();
        }

        // por defecto retorno el primer ID encontrado a menos que no exista
        if(intval($r->artists->total) == 0 ){
            throw new NotFoundException();
        }

        return array_shift($r->artists->items)->id;
    }

    /**
     * Obtiene los albums asociados al ID de un artista
     */
    public function getAlbums( $artista )
    {
        $id = $this->getArtistID( $artista );
        if( empty($id)){
            throw new NotFoundException();
        }

        $curl = new CurlHelper( $this->url_albums . $id . "/albums", $this->access_token, 'Bearer' );

        $r = json_decode( $curl->get() );
        if( isset($r->error) ){
            throw new BadRequestException();
        }
        
        $albums = [];
        // itero los albums
        foreach( $r->items as $item){
            
            $cover = array_shift($item->images);

            $a = new Album( $item->name, $item->release_date, $item->total_tracks, 
                            $cover->height, $cover->width, $cover->url);
            
            array_push($albums, $a->convert());
        }

        return $albums;
    }

    /**
     * Retorna la KEY
     */
    public function getKey() :string
    {
        return $this->key;
    }

    /**
     * Get the value of access_token
     */ 
    public function getAccessToken() : ?string
    {
        return $this->access_token;
    }

    /**
     * Get the value of token_type
     */ 
    public function getTokenType() : ?string
    {
        return $this->token_type;
    }

    /**
     * Get the value of expires_in
     */ 
    public function getExpiresIn() :?int
    {
        return $this->expires_in;
    }

    /**
     * Get the value of scope
     */ 
    public function getScope() : ?string
    {
        return $this->scope;
    }

    /**
     * Get the value of url_accounts
     */ 
    public function getUrlAccounts()
    {
        return $this->url_accounts;
    }

    /**
     * Get the value of url_search
     */ 
    public function getUrlSearch()
    {
        return $this->url_search;
    }   
}