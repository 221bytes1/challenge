<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

namespace Http;

use Exceptions\BadRequestException;

class CurlHelper {

    private $curl;

    public function __construct( $url, $key, $auth = 'Bearer' )
    {
        $this->curl = curl_init( $url );
        
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ["Authorization: $auth $key"] );
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER , true);    
        
    }

    /**
     * GET
     */
    public function get()
    {
        curl_setopt($this->curl, CURLOPT_POST, false);

        return $this->exec();
    }

    /**
     * POST
     */
    public function post( $data )
    {
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);

        return $this->exec();
    }

    /**
     * exec
     */
    private function exec()
    {
        $response = curl_exec($this->curl);
        if( $response === false ){
            throw new BadRequestException();
        }

        curl_close($this->curl);

        return $response;
    }
}