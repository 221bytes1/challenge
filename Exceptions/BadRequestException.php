<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

namespace Exceptions;

use Http\HttpCode;
use Exception;

class BadRequestException extends Exception
{
    protected $message;
    protected $code;

    public function __construct() {
    
        $this->message = HttpCode::getMessage( HttpCode::HTTP_BAD_REQUEST );
        $this->code = HttpCode::HTTP_BAD_REQUEST;

        parent::__construct( $this->message, $this->code, NULL);

    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function toResponse(){
        return ['error' => $this->code, 
                'message' => $this->message ];
    }
}