<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

use Exceptions\NotFoundException;
use Exceptions\BadRequestException;
use Http\HttpCode;
use Http\SpofityController;
use PHPUnit\Framework\TestCase;

class SpofityControllerTest extends TestCase
{

    public function testNuevo()
    {
        $sp = new SpofityController();

        $this->assertIsObject($sp);

        // tiene KEY?
        $this->assertNotEmpty($sp->getKey());
        $this->assertNotEquals($sp->getKey(), '');

        // tiene URL ACCOUNTS?
        $this->assertNotEmpty($sp->getUrlAccounts());
        $this->assertNotEquals($sp->getUrlAccounts(), '');

        // tiene URL SEARCH?
        $this->assertNotEmpty($sp->getUrlSearch());
        $this->assertNotEquals($sp->getUrlSearch(), '');

        // el resto de atributos tiene que estar en null
        $this->assertNull($sp->getAccessToken());
        $this->assertNull($sp->getTokenType());
        $this->assertNull($sp->getExpiresIn());
        $this->assertNull($sp->getScope());
    }

    /**
     * Obtengo el token 
     */
    public function testCallAccounts()
    {
        $sp = new SpofityController();

        $response = $sp->callAccount();

        $this->assertIsObject( $response );

        $this->assertNotNull($sp->getAccessToken());
        $this->assertNotNull($sp->getTokenType());
        $this->assertNotNull($sp->getExpiresIn());
        $this->assertNotNull($sp->getScope());

        $this->assertEquals($sp->getAccessToken(), $response->access_token );
        $this->assertEquals($sp->getTokenType(), $response->token_type );
        $this->assertEquals($sp->getExpiresIn(), $response->expires_in );
        $this->assertEquals($sp->getScope(), $response->scope );

    }

    /**
     * Obtengo el id del artista
     * 
     * @dataProvider additionProvider
     */
    public function testGetArtistID( $artista )
    {
        $sp = new SpofityController();

        try {
            $this->assertNotNull($sp->getArtistID( $artista ));
        } catch (NotFoundException $e){
            $this->assertEquals( $e->getCode(), HttpCode::HTTP_NOT_FOUND );

        } catch (BadRequestException $e){
            $this->assertEquals( $e->getCode(), HttpCode::HTTP_BAD_REQUEST );
        }
    }

    /**
     * Obtengo los albums del artista
     * 
     * @dataProvider additionProvider
     */
    public function testGetAlbums( $artista )
    {
        $sp = new SpofityController();

        try {
            $this->assertIsArray($sp->getAlbums( $artista ));

        } catch (NotFoundException $e){
            $this->assertEquals( $e->getCode(), HttpCode::HTTP_NOT_FOUND );

        } catch (BadRequestException $e){
            $this->assertEquals( $e->getCode(), HttpCode::HTTP_BAD_REQUEST );
        }
    }

    /**
     * Providers
     */
    public function additionProvider()
    {
        return [
            'van halen' => ["van halen"],
            'catupecu' => ["catupecu machu"],
            'metallica' => ["metallica"],
            'yo' => ["Jorge Huck"],
            'vacio' => [""],
        ];
    }
}