<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

use Models\Album;
use PHPUnit\Framework\TestCase;

class AlbumTest extends TestCase
{
    /**
     * Crea un nuevo album
     * 
     * @dataProvider additionProvider
     */
    public function testNuevo( $name, $released, $tracks, $height, $width, $url )
    {
        $album = new Album( $name, $released, $tracks, $height, $width, $url );
                            
        $this->assertEquals( $name, $album->getName() );
        
        $this->assertEquals( $released, $album->getReleased() );
        $this->assertEquals( $tracks, $album->getTracks() );

        $this->assertEquals( $height, $album->getCover()->getHeight() );
        $this->assertEquals( $width, $album->getCover()->getWidth() );
        $this->assertEquals( $url, $album->getCover()->getUrl() );

        // cómo funciona como json?
        $this->assertNotEquals( $album->convert(true), "" );

        $this->assertJsonStringEqualsJsonString(
            $album->convert(true),
            json_encode(
                [
                    "name" => $name,
                    "released" => $released,
                     "tracks" => $tracks,
                     "cover" => [
                         "height" => $height,
                         "width" => $width,
                         "url" => $url
                     ]
                ]
            )
        );
    }

    public function additionProvider()
    {
        return [
            'and justice for all' => [
                "...And Justice for All",
                "07-09-1988",
                9,
                300,
                300,
                "https://upload.wikimedia.org/wikipedia/en/b/bd/Metallica_-_...And_Justice_for_All_cover.jpg",
            ],
            'slippery when wet' => [
                "Slippery When Wet",
                "18-08-1986",
                10,
                0,
                0,
                null,
            ],
            'sultans of swing' => [
                "Sultans of Swing: The Very Best of Dire Straits",
                "",
                23,
                300,
                300,
                "https://upload.wikimedia.org/wikipedia/en/b/bf/Theverybestofdirestraits.jpg",
            ]
        ];

    }
}