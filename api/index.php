<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

use Http\HttpCode;
use Http\SpofityController;
use Exceptions\NotFoundException;
use Exceptions\BadRequestException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . './../vendor/autoload.php';

$app = new \Slim\App();

$app->get('/v1/albums', function (Request $req,  Response $res) {
    //return $res->withStatus(200)->write('funciona albums');
    $data = $req->getQueryParams();
    
    try {
        $sp = new SpofityController();

        return $res->withJson( $sp->getAlbums( $data['q'] ), HttpCode::HTTP_OK );

    } catch (NotFoundException $e){

        return $res->withJson( $e->toResponse(), HttpCode::HTTP_NOT_FOUND );

    }  catch (BadRequestException $e){

        return $res->withJson( $e->toResponse(), HttpCode::HTTP_BAD_REQUEST );

    } catch (Exception $e){
        
        return $res->withJson( ['error' => $e->getCode(), 
                                'message' => $e->getMessage() ], HttpCode::HTTP_INTERNAL_SERVER_ERROR );
    }

});

$app->run();