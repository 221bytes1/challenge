<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

namespace Models;

use Models\Cover;

class Album {

    private $name;
    private $released;
    private $tracks;
    private $cover;

    public function __construct( $name, $released, $tracks, $height, $width, $url ) {

        $this->name = $name;
        $this->released = $released;
        $this->tracks = $tracks;
        $this->cover = new Cover( $height, $width, $url );
        
    }    

    /**
     * Get the value of name
     */ 
    public function getName() :string
    {
        return $this->name;
    }

    /**
     * Get the value of released
     */ 
    public function getReleased() :string
    {
        return $this->released;
    }

    /**
     * Get the value of tracks
     */ 
    public function getTracks() :int
    {
        return $this->tracks;
    }

    /**
     * Get the value of cover
     */ 
    public function getCover() :Cover
    {
        return $this->cover;
    }

    /**
     * Retorna un json del objeto
     */
    public function convert( $to_json = false ){

        if( $this->released != ""){
            $aux = date_create( $this->released );
            $this->released = date_format( $aux, "d-m-Y" );
        }

        $data = [
            "name" => $this->name,
            "released" => $this->released,
            "tracks" => $this->tracks,
            "cover" => [
                "height" => $this->cover->getHeight(),
                "width" => $this->cover->getWidth(),
                "url" => $this->cover->getUrl()
            ]
        ];

        return $to_json ? json_encode( $data ) : $data;
    }
}