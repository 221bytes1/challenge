<?php

/**
 * 
 * Autor: jorgehuck@gmail.com
 * 
 */

namespace Models;

class Cover {

    private $height;
    private $width;
    private $url;
    
    function __construct( $height, $width, $url){
        $this->height = $height;
        $this->width = $width;
        $this->url = $url;
    }

    /**
     * Get the value of height
     */ 
    public function getHeight() :int
    {
        return $this->height;
    }

    /**
     * Get the value of width
     */ 
    public function getWidth() :int
    {
        return $this->width;
    }

    /**
     * Get the value of url
     */ 
    public function getUrl() :?string
    {
        return $this->url;
    }

}